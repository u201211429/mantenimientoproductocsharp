﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductoDao.Business
{
    public class UsuarioBusiness : GenericBusiness<Usuario>
    {
        public override Usuario Buscar(Usuario usuario)
        {
            Usuario usuarioTemp;
            using (var context = new productodbEntities())
            {
                try
                {
                    usuarioTemp = context.Usuarios
                        .Where(x => x.nombre == usuario.nombre && x.clave == usuario.clave)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return usuarioTemp;
        }

        public override Usuario Obtener(int id)
        {
            Usuario resultado = null;
            using (var context = new productodbEntities())
            {
                try
                {
                    resultado = context.Usuarios
                        .Where(x => x.idUsuario == id)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return resultado;
        }
    }
}
