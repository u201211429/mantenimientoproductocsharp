﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ProductoDao.Business
{
    public class CategoriaBusiness : GenericBusiness<Categoria>
    
    {
        public override Categoria Obtener(int id)
        {
            Categoria resultado = null;
            using (var context = new productodbEntities())
            {
                try
                {
                    resultado = context.Categorias
                        .Where(x => x.idCategoria == id)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return resultado;
        }

        public override Categoria Buscar(Categoria entidad)
        {
            Categoria categoriaTemp;
            using (var context = new productodbEntities())
            {
                try
                {
                    categoriaTemp = context.Categorias
                        .Where(x => x.nombre == entidad.nombre)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return categoriaTemp;
        }
    }
}