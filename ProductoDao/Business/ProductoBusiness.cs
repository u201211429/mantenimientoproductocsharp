﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductoDao.Business
{
    public class ProductoBusiness : GenericBusiness<Producto>
    {
        public override Producto Obtener(int id)
        {
            Producto resultado = null;
            using (var context = new productodbEntities())
            {
                try
                {
                    resultado = context.Productoes
                        .Include("Categoria")
                        .Where(x => x.idProducto == id)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return resultado;
        }
        public new List<Producto> Listar()
        {
            List<Producto> entidades = null;

            using (var context = new productodbEntities())
            {
                try
                {
                    entidades = context.Set<Producto>()
                        .Include("Categoria")
                        .Select(x => x)
                        .ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return entidades;
            
        }

        public override Producto Buscar(Producto entidad)
        {
            Producto productoTemp;
            using (var context = new productodbEntities())
            {
                try
                {
                    productoTemp = context.Productoes
                        .Where(x => x.nombre == entidad.nombre)
                        .Select(x => x)
                        .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return productoTemp;
            
        }
    }
}