﻿using MantenimientoProductoCSharp.Models;
using ProductoDao.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MantenimientoProductoCSharp.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Index/

        public ActionResult Ingreso()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioModel usuarioModel)
        {
            if (ModelState.IsValid) {
                UsuarioBusiness business = new UsuarioBusiness();
                Usuario usuario = new Usuario();
                usuario.clave = usuarioModel.clave;
                usuario.nombre = usuarioModel.nombre;
                if (business.Buscar(usuario) != null)
                {
                    
                    return RedirectToAction("Menu","Menu");
                }
            }
            ModelState.AddModelError("UsuarioError", "Datos incorrectos");
            return View("Ingreso");                
        }
    }
}
