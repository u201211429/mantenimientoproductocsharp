﻿using MantenimientoProductoCSharp.Models;
using MantenimientoProductoCSharp.ViewModel.Producto;
using ProductoDao.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MantenimientoProductoCSharp.Controllers
{
    public class ProductoController : Controller
    {
        //
        // GET: /Producto/

        public ActionResult Listar()
        {
            var viewModel = new ProductoListarViewModel();
            ProductoBusiness business = new ProductoBusiness();
            CategoriaBusiness businessCategoria = new CategoriaBusiness();
            viewModel.listaProducto = business.Listar().ConvertAll(x => new ProductoModel
            {
                idProducto = x.idProducto,
                stock = x.stock,
                idCategoria = x.idCategoria,
                fechaVencimiento = x.fechaVencimiento,
                categoria = x.Categoria,
                nombre = x.nombre
            });

            viewModel.listaCategoria = businessCategoria.Listar().ConvertAll(x => new SelectListItem 
            {
                Text = x.nombre,
                Value = x.idCategoria.ToString()
            });

            //Magia que permite pasar errores de un controller a otro
            ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);

            ViewBag.status = TempData["Status"];

            return View(viewModel);
        }

        public ActionResult Detalle(int? id)
        {
            ProductoBusiness business = new ProductoBusiness();
            if (id.HasValue)
            {
                ProductoModel model = (ProductoModel)business.Obtener(id.Value);
                return View(model);
            }
            return RedirectToAction("Listar");
        }

        public ActionResult Editar(int? id)
        {
            ProductoBusiness business = new ProductoBusiness();
            CategoriaBusiness businessCategoria = new CategoriaBusiness();
            var viewModel = new ProductoListarViewModel();

            viewModel.listaProducto = business.Listar().ConvertAll(x => new ProductoModel
            {
                idProducto = x.idProducto,
                stock = x.stock,
                idCategoria = x.idCategoria,
                fechaVencimiento = x.fechaVencimiento,
                categoria = x.Categoria,
                nombre = x.nombre
            });

            viewModel.listaCategoria = businessCategoria.Listar().ConvertAll(x => new SelectListItem
            {
                Text = x.nombre,
                Value = x.idCategoria.ToString()
            });

            ProductoModel producto = (ProductoModel)business.Obtener(id.Value);
            viewModel.producto = producto;

            @ViewBag.status = TempData["Status"];

            return View("Listar", viewModel);
        }
        public ActionResult Eliminar(int? id)
        {
            ProductoBusiness business = new ProductoBusiness();
            Producto producto = new Producto();
            producto.idProducto = id.GetValueOrDefault();
            try
            {
                business.Eliminar(producto);
                TempData["Status"] = "Se eliminó el producto satisfactoriamente.";
            }
            catch (Exception ex)
            {

                TempData["Status"] = "Ocurrió un error. ¿Existe el producto a eliminar?";
            }

            return RedirectToAction("Listar");
        }

        [HttpPost]
        public ActionResult InsertarEditar(ProductoListarViewModel viewModel)
        {
            ProductoBusiness business = new ProductoBusiness();
            CategoriaBusiness businessCategoria = new CategoriaBusiness();
            Producto producto = new Producto();
            producto = (Producto)viewModel.producto;

            if (businessCategoria.Obtener(producto.idCategoria) == null)
            {
                ModelState.AddModelError("ForeignKey","La categoría debe estar registrada en el sistema.");
            }

            if (ModelState.IsValid)
            {
                if (producto.idProducto == 0)
                {
                    viewModel.producto = (ProductoModel)business.Insertar(producto);
                    TempData["Status"] = "El producto ha sido insertado correctamente.";

                }
                else
                {
                    business.Actualizar(producto);
                    TempData["Status"] = "El producto se modificó correctamente.";
                }

                return RedirectToAction("Editar", new { id = viewModel.producto.idProducto });
            }

            TempData["ModelState"] = ModelState;
            return RedirectToAction("Listar");
        }
    }
}