﻿using MantenimientoProductoCSharp.Models;
using MantenimientoProductoCSharp.ViewModel.Categoria;
using ProductoDao.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MantenimientoProductoCSharp.Controllers
{
    public class CategoriaController : Controller
    {
        //
        // GET: /Producto/

        public ActionResult Listar()
        {
            CategoriaListarViewModel viewModel = new CategoriaListarViewModel();
            CategoriaBusiness business = new CategoriaBusiness();
            viewModel.listaCategoria = business.Listar().ConvertAll(x => new CategoriaModel
            {
                idCategoria = x.idCategoria,
                descripcion = x.descripcion,
                nombre = x.nombre
            });

            ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
            @ViewBag.status = TempData["Status"];

            return View(viewModel);
        }

        public ActionResult Detalle(int? id)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            if (id.HasValue)
            {
                CategoriaModel model = (CategoriaModel)business.Obtener(id.Value);
                return View(model);
            }
            return RedirectToAction("Listar");
        }

        public ActionResult Editar(int? id)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            var viewModel = new CategoriaListarViewModel();

            viewModel.listaCategoria = business.Listar().ConvertAll(x => new CategoriaModel
            {
                idCategoria = x.idCategoria,
                descripcion = x.descripcion,
                nombre = x.nombre
            });

            if (id.HasValue)
            {
                CategoriaModel categoria = (CategoriaModel)business.Obtener(id.Value);
                viewModel.categoria = categoria;
            }

            @ViewBag.status = TempData["Status"];

            return View("Listar", viewModel);
        }
        public ActionResult Eliminar(int? id)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.idCategoria = id.GetValueOrDefault();
            try
            {
                business.Eliminar(categoria);
                TempData["Status"] = "Se eliminó la categoría satisfactoriamente.";
            }
            catch (Exception ex)
            {
                TempData["Status"] = "Ocurrió un error. Puede que esta categoría este siendo usada por un producto";
            }

            return RedirectToAction("Listar");
        }

        [HttpPost]
        public ActionResult InsertarEditar(CategoriaListarViewModel viewModel)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria = (Categoria)viewModel.categoria;
            Categoria busqueda = business.Buscar(categoria);

            //Busqueda de repetidos en categoria
            if (busqueda != null)
            {
                TempData["Status"] = "La categoría " + categoria.nombre + " ya existe.";
                return RedirectToAction("Editar");
            }

            if (ModelState.IsValid)
            {
                if (categoria.idCategoria == 0)
                {
                    viewModel.categoria = (CategoriaModel)business.Insertar(categoria);
                    TempData["Status"] = "La categoría ha sido insertada correctamente.";

                }
                else
                {
                    business.Actualizar(categoria);
                    TempData["Status"] = "La categoría se modificó satisfactoriamente.";
                }

                return RedirectToAction("Editar", new { id = viewModel.categoria.idCategoria });
            }

            TempData["ModelState"] = ModelState;
            return RedirectToAction("Listar");
        }
    }
}
