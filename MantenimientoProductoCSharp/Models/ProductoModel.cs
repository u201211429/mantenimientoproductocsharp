﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ProductoDao.Business;

namespace MantenimientoProductoCSharp.Models
{
    public class ProductoModel
    {
        [Display(Name = "ID")]
        public int? idProducto { get; set; }

        [Required(ErrorMessage = "El nombre es un campo obligatorio.")]
        [Display(Name = "Nombre")]
        [StringLength(30,ErrorMessage="El nombre es más largo que lo permitido.")]
        public String nombre { get; set; }

        [Required(ErrorMessage="El stock es un campo obligatorio.")]
        [Display(Name = "Stock")]
        [Range(0, 10000,ErrorMessage="El stock debe ser valido.")]
        public int stock { get; set; }

        [Required(ErrorMessage="categoría es un campo obligatorio.")]
        [Display(Name = "Categoría")]
        public int idCategoria { get; set; }

        [Required(ErrorMessage="La fecha de vencimiento es obligatoria.")]
        [DataType(DataType.Date,ErrorMessage="La fecha debe estar en formato dd/mm/yyyy")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de vencimiento")]
        public DateTime fechaVencimiento { get; set; }

        public Categoria categoria { get; set; }


        public static explicit operator ProductoModel(Producto producto)
        {
            ProductoModel model = new ProductoModel();
            model.idProducto = producto.idProducto;
            model.nombre = producto.nombre;
            model.stock = producto.stock;
            model.idCategoria = producto.idCategoria;
            model.fechaVencimiento = producto.fechaVencimiento;
            model.categoria = producto.Categoria;
            return model;
        }

        public static explicit operator Producto(ProductoModel model)
        {
            Producto producto = new Producto();
            producto.idProducto = model.idProducto.GetValueOrDefault();
            producto.nombre = model.nombre;
            producto.idCategoria = model.idCategoria;
            producto.Categoria = model.categoria;
            producto.stock = model.stock;
            producto.fechaVencimiento = model.fechaVencimiento;
            return producto;
        }
    }
}