﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MantenimientoProductoCSharp.Models
{
    public class UsuarioModel
    {
        [Required(ErrorMessage="Se debe ingresar el nombre de usuario.")]
        [Display(Name = "Usuario")]
        [StringLength(30)]
        public String nombre { get; set; }

        [Required(ErrorMessage="Se debe ingresar la clave.")]
        [Display(Name = "Clave")]
        [StringLength(50, MinimumLength=8,ErrorMessage="La contraseña debe estar entre 8 y 50 caracteres")]
        public String clave { get; set; }
    }
}