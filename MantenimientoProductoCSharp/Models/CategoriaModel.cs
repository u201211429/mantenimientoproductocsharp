﻿using ProductoDao.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MantenimientoProductoCSharp.Models
{
    public class CategoriaModel
    {

        //Anotaciones magicas que hacen la validacion sin codigo

        [Display(Name="ID")]
        public int? idCategoria { get; set; }

        [Required(ErrorMessage="El nombre es un campo obligatorio.")]
        [Display(Name = "Nombre")]
        [StringLength(30,ErrorMessage="El nombre es más largo que lo permitido.")]
        public String nombre { get; set; }

        [Display(Name = "Descripcion")]
        [StringLength(200)]
        public String descripcion { get; set; }

        public static explicit operator CategoriaModel(Categoria categoria)
        {
            CategoriaModel model = new CategoriaModel();
            model.idCategoria = categoria.idCategoria;
            model.nombre = categoria.nombre;
            model.descripcion = categoria.descripcion;
            return model;
        }

        public static explicit operator Categoria(CategoriaModel model)
        {
            Categoria categoria = new Categoria();
            categoria.idCategoria = model.idCategoria.GetValueOrDefault();
            categoria.descripcion = model.descripcion;
            categoria.nombre = model.nombre;
            return categoria;
        }
    }
}