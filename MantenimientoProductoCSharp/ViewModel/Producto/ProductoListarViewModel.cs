﻿using MantenimientoProductoCSharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MantenimientoProductoCSharp.ViewModel.Producto
{
    public class ProductoListarViewModel
    {
        public ProductoModel producto { get; set; }
        public List<ProductoModel> listaProducto { get; set; }
        public List<SelectListItem> listaCategoria { get; set; }
    }
}