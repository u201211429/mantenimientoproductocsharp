﻿using MantenimientoProductoCSharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MantenimientoProductoCSharp.ViewModel.Categoria
{
    public class CategoriaListarViewModel
    {
        public CategoriaModel categoria { get; set; }
        public List<CategoriaModel> listaCategoria { get; set; }
    }
}