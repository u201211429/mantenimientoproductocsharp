﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductoDao.Business;
using MantenimientoProductoCSharp.Models;
using System.Collections.Generic;

namespace ProductoDao.Business.Tests
{
    [TestClass]
    public class CategoriaBusinessTest
    {

        private Categoria categoriaTest;

        [TestMethod]
        public void TestListarCategoria()
        {
            Categoria categoria = new Categoria();
            List<Categoria> lista = null;
            categoria.nombre = "TestListar";
            categoria.descripcion = "TestListar";

            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);
            lista = business.Listar();


            Assert.IsNotNull(lista);
        }

        [TestMethod]
        public void TestObtenerCategoria()
        {
            Categoria categoria = new Categoria();
            Categoria obtener = null;
            categoria.nombre = "TestObtener";
            categoria.descripcion = "TestObtener";

            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);
            obtener = business.Obtener(categoriaTest.idCategoria);


            Assert.IsNotNull(obtener);
        }

        [TestMethod]
        public void TestActualizarCategoria()
        {
            Categoria categoria = new Categoria();
            Categoria obtener = null;
            categoria.nombre = "TestObtener";
            categoria.descripcion = "TestObtener";

            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);
            categoria.idCategoria = categoriaTest.idCategoria;
            categoria.nombre = "TestActualizar2";
            business.Actualizar(categoria);
            obtener = business.Obtener(categoriaTest.idCategoria);

            Assert.AreEqual(categoria.nombre, obtener.nombre);
        }

        [TestMethod]
        public void TestEliminarCategoria()
        {
            Categoria categoria = new Categoria();
            Categoria obtener = null;
            categoria.nombre = "TestEliminar";
            categoria.descripcion = "TestEliminar";

            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);
            business.Eliminar(categoria);
            obtener = business.Obtener(categoriaTest.idCategoria);

            //Para evitar ser borrado en cleanup
            categoriaTest.idCategoria = 0;


            Assert.IsNull(obtener);
        }

        [TestMethod]
        public void TestInsertarCategoria()
        {
            Categoria categoria = new Categoria();
            categoria.nombre = "TestInsertar";
            categoria.descripcion = "TestInsertar";

            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);

            Assert.AreNotEqual(0, categoriaTest);
        }

        [TestMethod]
        public void TestBuscarCategoria()
        {
            Categoria categoria = new Categoria();
            categoria.nombre = "TestInsertar";
            Categoria obtener = null;
            categoria.descripcion = "TestInsertar";
            CategoriaBusiness business = new CategoriaBusiness();

            categoriaTest = business.Insertar(categoria);
            obtener = business.Buscar(categoriaTest);


            Assert.IsNotNull(obtener);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void TestEliminarCategoriaError()
        {
            Categoria categoria = new Categoria();
            categoria.nombre = "TestEliminarError";
            categoria.descripcion = "TestEliminarError";

            CategoriaBusiness business = new CategoriaBusiness();
            categoriaTest = business.Insertar(categoria);
            business.Eliminar(null);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void TestActualizarCategoriaError()
        {
            Categoria categoria = new Categoria();
            categoria.nombre = "TestActualizarError";
            categoria.descripcion = "TestActualizarError";

            CategoriaBusiness business = new CategoriaBusiness();
            categoriaTest = business.Insertar(categoria);
            business.Actualizar(null);
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (categoriaTest.idCategoria != 0)
            {
                CategoriaBusiness business = new CategoriaBusiness();
                Categoria categoria;

                categoria = business.Obtener(categoriaTest.idCategoria);
                business.Eliminar(categoria);


            }
            categoriaTest.idCategoria = 0;
        }
    }
}