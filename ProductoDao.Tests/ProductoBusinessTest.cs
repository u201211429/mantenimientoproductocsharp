﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductoDao.Business;
using MantenimientoProductoCSharp.Models;
using System.Collections.Generic;

namespace ProductoDao.Business.Tests
{
    [TestClass]
    public class ProductoBusinessTest
    {

        private Producto productoTest;
        private Categoria categoriaTest;

        [TestMethod]
        public void TestListarProducto()
        {
            Producto producto = new Producto();
            List<Producto> lista = null;
            producto.nombre = "TestListar";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;

            ProductoBusiness business = new ProductoBusiness();

            productoTest = business.Insertar(producto);
            lista = business.Listar();


            Assert.IsNotNull(lista);
        }

        [TestMethod]
        public void TestObtenerProducto()
        {
            Producto producto = new Producto();
            producto.nombre = "TestObtener";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;

            Producto obtener;


            ProductoBusiness business = new ProductoBusiness();
            productoTest = business.Insertar(producto);
            obtener = business.Obtener(productoTest.idProducto);


            Assert.IsNotNull(obtener);
        }

        [TestMethod]
        public void TestActualizarProducto()
        {
            Producto producto = new Producto();
            producto.nombre = "TestActualizar";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;

            Producto obtener = new Producto();

            ProductoBusiness business = new ProductoBusiness();

            productoTest = business.Insertar(producto);
            producto.idCategoria = productoTest.idCategoria;
            producto.nombre = "TestActualizar2";
            business.Actualizar(producto);
            obtener = business.Obtener(productoTest.idProducto);

            Assert.AreEqual(producto.nombre, obtener.nombre);
        }

        [TestMethod]
        public void TestEliminarProducto()
        {
            Producto producto = new Producto();
            producto.nombre = "TestEliminar";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;

            Producto obtener = new Producto();

            ProductoBusiness business = new ProductoBusiness();
            productoTest = business.Insertar(producto);
            business.Eliminar(producto);
            obtener = business.Obtener(productoTest.idProducto);
            productoTest.idCategoria = 0;
            Assert.IsNull(obtener);
        }

        [TestMethod]
        public void TestInsertarProducto()
        {
            Producto producto = new Producto();
            producto.nombre = "TestInsertar";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;

            ProductoBusiness business = new ProductoBusiness();
            productoTest = business.Insertar(producto);
            Assert.AreNotEqual(0, productoTest);
        }

        [TestMethod]
        public void TestBuscarProducto()
        {
            Producto producto = new Producto();
            Producto obtener;  
            producto.nombre = "TestInsertar";
            producto.stock = 30;
            producto.fechaVencimiento = DateTime.Now;
            producto.idCategoria = categoriaTest.idCategoria;
            ProductoBusiness business = new ProductoBusiness();

            productoTest = business.Insertar(producto);
                      
            obtener = business.Buscar(productoTest);
            Assert.IsNotNull(obtener);
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (productoTest.idCategoria != 0)
            {
                ProductoBusiness businessProducto = new ProductoBusiness();
                CategoriaBusiness businessCategoria = new CategoriaBusiness();
                Producto producto;
                Categoria categoria;

                producto = businessProducto.Obtener(productoTest.idProducto);
                businessProducto.Eliminar(producto);
                categoria = businessCategoria.Obtener(categoriaTest.idCategoria);
                businessCategoria.Eliminar(categoria);

            }
            productoTest.idCategoria = 0;
            categoriaTest.idCategoria = 0;
        }


        [TestInitialize]
        public void Initialize()
        {
            Categoria categoria = new Categoria();
            CategoriaBusiness businessCategoria = new CategoriaBusiness();
            categoria.nombre = "Test";
            categoria.descripcion = "Test";

            categoriaTest = businessCategoria.Insertar(categoria);
        }
    }
}