﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProductoDao.Business.Tests
{
    [TestClass]
    public class UsuarioBusinessTest
    {

        private Usuario usuarioTest;

        [TestMethod]
        public void TestBuscarUsuario()
        {
            Usuario usuario = new Usuario();
            Usuario obtener = null;
            usuario.nombre = "TestBuscar";
            usuario.clave = "TestBuscar";

            UsuarioBusiness business = new UsuarioBusiness();

            usuarioTest = business.Insertar(usuario);
            obtener = business.Buscar(usuarioTest);

            Assert.IsNotNull(obtener);
        }
        [TestMethod]
        public void TestObtenerUsuario()
        {
            Usuario usuario = new Usuario();
            Usuario obtener = null;
            usuario.nombre = "TestObtener";
            usuario.clave = "TestObtener";

            UsuarioBusiness business = new UsuarioBusiness();

            usuarioTest = business.Insertar(usuario);
            obtener = business.Obtener(usuarioTest.idUsuario);



            Assert.IsNotNull(obtener);
        }
        [TestMethod]
        public void TestInsertarUsuario()
        {
            Usuario usuario = new Usuario();
            usuario.nombre = "TestObtener";
            usuario.clave = "TestObtener";

            UsuarioBusiness business = new UsuarioBusiness();

            usuarioTest = business.Insertar(usuario);


            Assert.IsNotNull(usuarioTest);
        }
        [TestMethod]
        public void TestEliminarUsuario()
        {
            Usuario usuario = new Usuario();
            Usuario obtener = null;
            usuario.nombre = "TestEliminar";
            usuario.clave = "TestEliminar";

            UsuarioBusiness business = new UsuarioBusiness();

            usuarioTest = business.Insertar(usuario);
            business.Eliminar(usuarioTest);
            obtener = business.Obtener(usuarioTest.idUsuario);

            //Para evitar ser borrado en cleanup
            usuarioTest.idUsuario = 0;


            Assert.IsNull(obtener);
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (usuarioTest.idUsuario != 0)
            {
                UsuarioBusiness business = new UsuarioBusiness();
                Usuario usuario;

                usuario = business.Obtener(usuarioTest.idUsuario);
                business.Eliminar(usuario);

            }
            usuarioTest.idUsuario = 0;
        }
    }
}
