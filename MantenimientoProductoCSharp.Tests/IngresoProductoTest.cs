﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductoDao.Business;
using Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class IngresoProductoTest
    {
        private DefaultSelenium driver;
        int idCategoria;
        int idProducto;

        [TestMethod]
        public void IngresarProductoOK()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "ingresarProductoOK";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Regex r = new Regex("\\d+");
            MatchCollection m = r.Matches(driver.GetLocation());
            Group g = m[1];
            idProducto = Convert.ToInt32(g.Value);

            Assert.IsTrue(driver.IsTextPresent("El producto ha sido insertado correctamente."));
        }

        [TestMethod]
        public void ErrorFechaInvalida()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorFechaInvalida";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "2015-2-1");
            driver.Click("btnAceptar");

            Assert.IsTrue(driver.IsTextPresent("La fecha debe estar en formato dd/mm/yyyy."));
        }

        [TestMethod]
        public void ErrorFechaNula()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorFechaNula";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "");
            driver.Click("btnAceptar");

            Assert.IsTrue(driver.IsTextPresent("La fecha de vencimiento es obligatoria."));
        }

        [TestMethod]
        public void ErrorStockFueraRango()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorStockFueraRango";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "-1");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El stock debe ser valido."));
        }

        [TestMethod]
        public void ErrorStockNulo()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorStockNulo";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El stock es un campo obligatorio."));
        }


        /*
        [TestMethod]
        public void errorCategoriaNula()
        {

            driver.open("/Menu/Menu");
            driver.click("btnProductos");
            driver.waitForPageToLoad("30000");
            driver.type("txtNombreProducto", "Inka Inglesa");
            driver.type("txtStock", "7");
            driver.runScript("document.getElementById('txtCategoria').value = '999999'");
            driver.type("txtFecha", "01/05/2015");
            driver.submit("frmProducto");

            driver.waitForPageToLoad("30000");
            Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría es un campo obligatorio.")));
        }


        [TestMethod]
        public void errorCategoriaNoExiste()
        {

            driver.open("/Menu/Menu");
            driver.click("btnProductos");
            driver.waitForPageToLoad("30000");
            driver.type("txtNombreProducto", "Inka Inglesa");
            driver.type("txtStock", "7");
            driver.type("txtCategoria", "19999");
            driver.type("txtFecha", "01/05/2015");
            driver.submit("frmProducto");

            driver.waitForPageToLoad("30000");
            Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría debe estar registrada en el sistema.")));
        }
        */

        [TestMethod]
        public void ErrorNombreNulo()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorNombreNulo";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El nombre es un campo obligatorio."));
        }


        [TestMethod]
        public void ErrorNombreFueraRango()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorNombreFueraRango";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.RunScript("document.getElementById('producto_nombre').value = 'Inka cola inglesa pepsi fanta concordia'");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El nombre es más largo que lo permitido."));
        }

        [TestMethod]
        public void ActualizarProductoOK()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "actualizarProductoOK";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");
            Assert.IsTrue(driver.IsTextPresent("El producto se modificó correctamente."));
        }

        [TestMethod]
        public void ErrorFechaInvalidaActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorFechaInvalidaActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "2015/1/1");
            driver.Click("btnAceptar");

            Assert.IsTrue(driver.IsTextPresent("La fecha debe estar en formato dd/mm/yyyy."));
        }


        [TestMethod]
        public void ErrorFechaNulaActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorFechaNulaActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "");
            driver.Click("btnAceptar");

            Assert.IsTrue(driver.IsTextPresent("La fecha de vencimiento es obligatoria."));
        }

        [TestMethod]
        public void ErrorStockNuloActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorStockNuloActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El stock es un campo obligatorio."));
        }

        [TestMethod]
        public void ErrorNombreNuloActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorNombreNuloActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El nombre es un campo obligatorio."));
        }

        [TestMethod]
        public void ErrorNombreFueraRangoActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorNombreFueraRangoActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.RunScript("document.getElementById('producto_nombre').value = 'Inka cola inglesa pepsi fanta concordia'");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("El nombre es más largo que lo permitido."));
        }

        /*

        [TestMethod]
        public void errorCategoriaNoExisteActualizar()
        {

            driver.open("/ProductoWeb/menu.jsp");
            driver.click("btnProductos");
            driver.waitForPageToLoad("30000");
            driver.click("btnEditar" + idProducto);
            driver.waitForPageToLoad("30000");
            driver.type("txtNombreProducto", "Inka Inglesa");
            driver.type("txtStock", "7");
            driver.type("txtCategoria", "19999");
            driver.type("txtFecha", "01/05/2015");
            driver.submit("frmProducto");

            driver.waitForPageToLoad("30000");
            Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría debe estar registrada en el sistema.")));
        }



        [TestMethod]
        public void errorCategoriaNulaActualizar()
        {

            driver.open("/ProductoWeb/menu.jsp");
            driver.click("btnProductos");
            driver.waitForPageToLoad("30000");
            driver.click("btnEditar" + idProducto);
            driver.waitForPageToLoad("30000");
            driver.type("txtNombreProducto", "Inka Inglesa");
            driver.type("txtStock", "7");
            driver.type("txtCategoria", " ");
            driver.type("txtFecha", "01/05/2015");
            driver.submit("frmProducto");

            driver.waitForPageToLoad("30000");
            Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría es un campo obligatorio.")));
        }
        
         */


        [TestMethod]
        public void ErrorStockFueraDeRangoActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "errorStockFueraDeRangoActualizar";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            ProductoBusiness businessProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Prueba";
            producto.stock = 7;
            producto.idCategoria = idCategoria;
            producto.fechaVencimiento = DateTime.Now;

            producto = businessProducto.Insertar(producto);
            idProducto = producto.idProducto;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idProducto);
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "-1");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");
            Assert.IsTrue(driver.IsTextPresent("El stock debe ser valido."));
        }


        [TestMethod]
        public void EliminarProductoOK()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "eliminarProductoOK";
            categoria = business.Insertar(categoria);
            idCategoria = categoria.idCategoria;

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Type("producto_nombre", "Inka Inglesa");
            driver.Type("producto_stock", "2000");
            driver.Type("producto_idCategoria", idCategoria.ToString());
            driver.Type("producto_fechaVencimiento", "01/05/2015");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Regex r = new Regex("\\d+");
            MatchCollection m = r.Matches(driver.GetLocation());
            Group g = m[1];
            idProducto = Convert.ToInt32(g.Value);

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEliminar" + idProducto);
            driver.WaitForPageToLoad("30000");
            idProducto = 0;

            Assert.IsTrue(driver.IsTextPresent("Se eliminó el producto satisfactoriamente."));
        }


        [TestInitialize]
        public void Initialize()
        {
            idCategoria = 0;
            idProducto = 0;
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();

        }

        [TestCleanup]
        public void Cleanup()
        {
            if (idProducto != 0)
            {
                ProductoBusiness businessProducto = new ProductoBusiness();
                businessProducto.Eliminar(businessProducto.Obtener(idProducto));
                driver.Stop();
            }
            if (idCategoria != 0)
            {
                CategoriaBusiness business = new CategoriaBusiness();
                business.Eliminar(business.Obtener(idCategoria));
            }
        }
    }
}
