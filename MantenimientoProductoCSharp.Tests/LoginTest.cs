﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Selenium;
using System.Text.RegularExpressions;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class LoginTest
    {
        private DefaultSelenium driver;

        [TestMethod]
        public void LoginOK()
        {
            driver.Open("/");
            driver.Type("nombre", "admin");
            driver.Type("clave", "adminadmin");
            driver.Click("btnIngresar");
            driver.WaitForPageToLoad("30000");
            Assert.IsTrue(Regex.IsMatch(driver.GetLocation(), "Menu/Menu"));
        }

        [TestMethod]
        public void ErrorUsuarioEmpleadoNulo()
        {

            driver.Open("/");
            driver.Type("clave", "adminadmin");
            driver.Click("btnIngresar");
            Assert.IsTrue(driver.IsTextPresent("Se debe ingresar el nombre de usuario."));

        }

        [TestMethod]
        public void ErrorClaveEmpleadoNulo()
        {
            driver.Open("/");
            driver.Type("nombre", "adminadmin");
            driver.Click("btnIngresar");
            Assert.IsTrue(driver.IsTextPresent("Se debe ingresar la clave."));
        }

        [TestMethod]
        public void ErrorUsuarioYclaveIncorrectos()
        {

            driver.Open("/");
            driver.Type("nombre", "user01");
            driver.Type("clave", "12345678");
            driver.Click("btnIngresar");

            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("Datos incorrectos"));

        }

        [TestInitialize]
        public void Initialize()
        {
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();
        }

        [TestCleanup]
        private void CVleanup()
        {
            driver.Stop();
        }
    }
}
