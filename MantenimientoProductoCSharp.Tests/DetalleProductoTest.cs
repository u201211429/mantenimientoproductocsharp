﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductoDao.Business;
using Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class DetalleProductoTest
    {
        private DefaultSelenium driver;
        private Producto productoGeneral;
        private Categoria categoriaGeneral;

        [TestMethod]
        public void DetalleProductoOK()
        {

            driver.Open("/Menu/Menu");
            driver.Click("btnProductos");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnDetalle" + productoGeneral.idProducto);
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("Detalle de producto"));
        }

        [TestInitialize]
        public void beforeMethod()
        {
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();


            CategoriaBusiness daoCategoria = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "PruebaDetalleProducto";
            categoria.descripcion = "";
            categoriaGeneral = daoCategoria.Insertar(categoria);

            ProductoBusiness daoProducto = new ProductoBusiness();
            Producto producto = new Producto();
            producto.nombre = "Inka Inglesa";
            producto.stock = 2000;
            producto.idCategoria = categoriaGeneral.idCategoria;
            producto.fechaVencimiento = DateTime.Today;
            productoGeneral = daoProducto.Insertar(producto);
        }

        [TestCleanup]
        public void afterMethod()
        {

            driver.Stop();
            ProductoBusiness daoProducto = new ProductoBusiness();
            daoProducto.Eliminar(productoGeneral);
            CategoriaBusiness daoCategoria = new CategoriaBusiness();
            daoCategoria.Eliminar(categoriaGeneral);
        }
    }
}