﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Selenium;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductoDao.Business;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class DetalleCategoriaTest
    {

        private DefaultSelenium driver;
        private Categoria categoriaGeneral;

        [TestMethod]
        public void DetalleCategoriaOK()
        {
            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnDetalle" + categoriaGeneral.idCategoria);
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("Detalle de categoría"));
        }

        [TestInitialize]
        public void beforeMethod()
        {
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();

            CategoriaBusiness dao = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "PruebaDetalle";
            categoria.descripcion = "";
            categoriaGeneral = dao.Insertar(categoria);
        }

        [TestCleanup]
        public void afterMethod()
        {
            driver.Stop();
            CategoriaBusiness dao = new CategoriaBusiness();
            dao.Eliminar(categoriaGeneral);
        }
    }
}