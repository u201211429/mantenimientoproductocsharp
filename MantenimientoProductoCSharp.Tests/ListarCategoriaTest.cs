﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class ListarCategoriaTest
    {
        private DefaultSelenium driver;


        [TestMethod]
        public void ListarProductosOK()
        {

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("Ingresar o editar categorías"));

        }

        [TestInitialize]
        public void BeforeMethod()
        {
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();

        }

        [TestCleanup]
        public void AfterMethod()
        {
            driver.Stop();
        }
    }
}
