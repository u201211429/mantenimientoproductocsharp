﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Selenium;
using System.Text.RegularExpressions;
using ProductoDao.Business;

namespace MantenimientoProductoCSharp.Tests
{
    [TestClass]
    public class IngresoCategoriasTest
    {
        private DefaultSelenium driver;
        private List<int> idCategoria = new List<int>();

        [TestMethod]
        public void IngresarCategoriaOK() {

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "Lácteos");
            driver.Type("categoria_descripcion", "Productos provenientes de leche");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Regex r = new Regex("\\d+");
            MatchCollection m = r.Matches(driver.GetLocation());
            Group g = m[1];
            idCategoria.Add(Convert.ToInt32(g.Value));

            Assert.IsTrue(driver.IsTextPresent("La categoría ha sido insertada correctamente."));
        }


        [TestMethod]
        public void ErrorNombreNuloInsertar()
        {
            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_descripcion", "Productos provenientes de leche");
            driver.Click("btnAceptar");
            Assert.IsTrue(driver.IsTextPresent("El nombre es un campo obligatorio."));

        }

        [TestMethod]
        public void ErrorNombreRangoInsertar()
        {
            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "Lácteos y quesos importados variados");
            driver.Type("categoria_descripcion", "Productos provenientes de leche");
            driver.Click("btnAceptar");
            Assert.IsTrue(driver.IsTextPresent("El nombre es más largo que lo permitido."));

        }

        [TestMethod]
        public void ErrorNombreYaExisteInsertar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "Lácteos");
            driver.Type("categoria_descripcion", "Productos provenientes de leche");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("La categoría Lácteos ya existe."));

        }

        //Aca van las de actualizacion
        [TestMethod]
        public void ActualizarCategoriaOK()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idCategoria.First<int>());
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "Licores");
            driver.Type("categoria_descripcion", "Bebidas alcohólicas +18");
            driver.Click("btnAceptar");

            driver.WaitForPageToLoad("30000");
            Assert.IsTrue(driver.IsTextPresent("La categoría se modificó satisfactoriamente."));
        }

        //Modificar categoria error
        [TestMethod]
        public void ErrorNombreNuloActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idCategoria.First<int>());
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "");
            driver.Type("categoria_descripcion", "Productos provenientes de la leche");
            driver.Click("btnAceptar");
            Assert.IsTrue(driver.IsTextPresent("El nombre es un campo obligatorio."));

        }

        [TestMethod]
        public void ErrorNombreRangoActualizar()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idCategoria.First<int>());
            driver.WaitForPageToLoad("30000");
            driver.Type("categoria_nombre", "Lácteos y quesos importados variados");
            driver.Type("categoria_descripcion", "Productos provenientes de la leche");
            driver.Click("btnAceptar");

            Assert.IsTrue(driver.IsTextPresent("El nombre es más largo que lo permitido."));

        }

        [TestMethod]
        public void EliminarCategoriaOK()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEliminar" + idCategoria.First<int>());

            driver.WaitForPageToLoad("30000");

            //Para evitar que intente borrar la categoria que no existe
            idCategoria.Clear();

            Assert.IsTrue(driver.IsTextPresent("Se eliminó la categoría satisfactoriamente."));

        }

        [TestMethod]
        public void ErrorNombreYaExisteActualizar()
        {
            //Primera categoria
            CategoriaBusiness business = new CategoriaBusiness();
            Categoria categoria = new Categoria();
            categoria.nombre = "Licores";
            categoria.descripcion = "";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            //Segunda categoria
            categoria.nombre = "Lácteos";
            categoria.descripcion = "Productos provenientes de leche";

            categoria = business.Insertar(categoria);
            idCategoria.Add(categoria.idCategoria);

            driver.Open("/Menu/Menu");
            driver.Click("btnCategoria");
            driver.WaitForPageToLoad("30000");
            driver.Click("btnEditar" + idCategoria[1]);
            driver.WaitForPageToLoad("30000");

            driver.Type("categoria_nombre", "Licores");
            driver.Type("categoria_descripcion", "Productos provenientes de la leche");
            driver.Click("btnAceptar");
            driver.WaitForPageToLoad("30000");

            Assert.IsTrue(driver.IsTextPresent("La categoría Licores ya existe."));
        }

        [TestInitialize]
        public void Initialize()
        {
            idCategoria.Clear();
            driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:21686/");
            driver.Start();

        }

        [TestCleanup]
        public void Cleanup()
        {
            foreach (int id in idCategoria) {
                CategoriaBusiness business = new CategoriaBusiness();
                business.Eliminar(business.Obtener(id));
            }
            driver.Stop();
        }
    }
}
